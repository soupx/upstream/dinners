
= Zippy Cheese Artichoke Oven Omelet

----

Recipe By     : 
Serving Size  : 6     Preparation Time : 0:15
Categories    : Side Dish                        
  Amount  Measure       Ingredient -- Preparation Method
--------  ------------  --------------------------------
     3/4           cup  picante sauce or salsa -- hot or mild
       1           cup  artichoke hearts -- chopped
     1/4           cup  (1 ounce) Parmesan cheese -- grated
       1           cup  (4 ounces) Monterey Jack cheese -- shredded
       1           cup  (4 ounces) sharp Cheddar cheese -- shredded
       6         large  eggs
       1  (8 ounce) ca  sour cream
                        Tomato wedges -- optional
                        Parsley sprigs -- optional

Preheat oven to 350 F. Butter a 10-inch quiche dish. Spread the picante sauce on the bottom. Distribute the chopped artichokes evenly over the picante sauce.

Sprinkle Parmesan cheese over the artichokes. Sprinkle with Monterey Jack and Cheddar cheese. Blend the eggs in a blender until smooth. Add the sour cream to the eggs and blend until mixed.

Pour the egg mixture over the cheeses. Bake uncovered for 30 to 40 minutes, or until set. Cut into wedges and serve garnished with tomato wedges and parsley.

Source:
  "American Dairy Association"
S(Internet Address):
  "http://www.ilovecheese.com/"
T(Cook Time):
  "0:35"



                    
                                    - - - - - - - - - - - - - - - - - - - 




Nutr. Assoc. : 0
----
